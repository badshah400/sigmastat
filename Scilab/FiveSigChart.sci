exec("Sig2ChiSq.sci", -1);

function y = FiveSigChart(dofs)
    sigs = [1:5];
    mprintf("#DOF");
    mprintf("%13dσ", sigs');
    mprintf("\n");
    for d = dofs
        mprintf("%3d ", d);
        chis = Sig2ChiSq(sigs, d);
        mprintf("%14.5f", chis');
        mprintf("\n");
    end
    y = 0
endfunction
