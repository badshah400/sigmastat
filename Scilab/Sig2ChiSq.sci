function y = Sig2ChiSq(sigs, dof)
    if ~ isscalar(dof) then
        error ("Degree(s) of freedom has to be a scalar.")
    end
    erfs = erf(sigs ./ sqrt(2.0));
    y    = cdfchi("X", repmat(dof, [1,length(sigs)]), erfs, 1.0-erfs);
endfunction
