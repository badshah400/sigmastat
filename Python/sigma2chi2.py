#!/usr/bin/python3
# vim: set ai et ts=4 sw=4 tw=80:

from scipy.stats import chi2
from scipy.special import erf
from math import *
from decimal import *

def Sig2ChiSq(sig, df=1):
    '''Given a value of sigma and the d.o.f (default: 1)
       return the corresponding value of chi square.
    '''
    contxt = getcontext()
    contxt.prec=50
    pc = erf(Decimal(sig,contxt)/Decimal(2.0).sqrt(contxt))
    return Decimal(chi2.ppf(pc, int(df)), contxt)

def Conf2ChiSq(conf, df=1):
    '''Given a confidence level value (e.g. 0.90 for 90%) and the d.o.f
       (default: 1), return the corresponding value of chi square.
    '''
    contxt = getcontext()
    contxt.prec=50
    return Decimal(chi2.ppf(conf, int(df)), contxt)
    
def FiveSigChart(dofs=range(1,6)):
    '''For a list of dofs (default: 1->5) print the 5 sigma chart.
    '''
    sigmas  = range(1,6)
    erfs    = [erf(x/sqrt(2)) for x in sigmas]
    heads   = [u'{:-14d}σ'.format(s) for s in sigmas]
    headstr = ''.join(heads)
    headstr = '#DOF{:s}'.format(headstr)
    print(headstr)

    for dof in dofs:
        chi2s = ['{:15.5f}'.format(chi2.ppf(x, dof)) for x in erfs]
        print('{:3d} {:s}'.format(dof, ''.join(chi2s)))
    return

def ConfLevelChart(confs=[0.67, .90, 0.95, 0.99], dofs=range(1,6)):
    '''For a list of confidence levels (default: 67%, 90%, 95%, 99%) print the
       corresponding chi2 for given d.o.fs (default: 1->5) as a chart.
    '''
    heads   = ['{:-14.1f}%'.format(x*100) for x in confs]
    headstr = ''.join(heads)
    headstr = '#DOF{:s}'.format(headstr)
    print(headstr)

    for dof in dofs:
        chi2s = ['{:15.5f}'.format(chi2.ppf(x, dof)) for x in confs]
        print('{:3d} {:s}'.format(dof, ''.join(chi2s)))
    return

if __name__ == '__main__':
    FiveSigChart()
    ConfLevelChart()
