#!/usr/bin/octave -qf
% vim: set ai et tw=80 ts=4 sw=4:

function y = Sig2ChiSq(sigs, dof=1)
	y = 0;
	if ! isscalar(dof)
		error("Scalar expected for degree(s) of freedom.\n");
	endif
	erfs = erf(sigs ./ sqrt(2));
	y = chi2inv(erfs, dof);
endfunction


