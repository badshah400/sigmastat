#!/usr/bin/octave -qf
% vim: set ai et ts=4 sw=4 tw=80:

function y = FiveSigChart(dofs=[1:5])
    printf("#DOF");
    for i = 1:5
        printf("%14iσ", i);
    endfor
    printf("\n");

    for dof = dofs
        chi2s = chi2inv(erf([1:5] ./ sqrt(2)), dof);
        printf("%3d ", dof);
        printf("%15.5f",chi2s);
        printf("\n")
    endfor

endfunction

