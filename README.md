#Sigma2Chi2#

Utilities in various languages to quickly lookup the value of χ² corresponding
to a specified σ and arbitrary degrees of freedom.
